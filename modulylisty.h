struct Client
{
    char nazwisko[30];
    struct Client *next;
};
int list_size(struct Client *head);
void show_list(struct Client *head);
void push_back(struct Client **head, char *p_nazwisko);
void pop_by_surname(struct Client **head, char *p_nazwisko);